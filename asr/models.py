# asr/model.py

import tensorflow as tf
from tensorflow import keras


class ConvBlock(keras.layers.Layer):
    def __init__(self):
        super(ConvBlock, self).__init__()
        self.conv_1 = keras.layers.Conv2D(
            filters=32,
            kernel_size=[11, 41],
            strides=[2, 2],
            padding="same",
            use_bias=False,
        )
        self.batch_norm_1 = keras.layers.BatchNormalization()
        self.conv_2 = keras.layers.Conv2D(
            filters=32,
            kernel_size=[11, 21],
            strides=[1, 2],
            padding='same',
            use_bias=False,
        )
        self.batch_norm_2 = keras.layers.BatchNormalization()

    def call(self, inputs):
        x = self.conv_1(inputs)
        x = self.batch_norm_1(x)
        x = tf.nn.relu(x)
        x = self.conv_2(x)
        x = self.batch_norm_2(x)
        return tf.nn.relu(x)


#  TODO flexibility

def initialize_model(
        input_dim: int,
        output_dim: int,
        rnn_layers: int,
        rnn_dims: int,
        fc_dims: int,
        dropout_rate: float,
):
    input_spectrogram = keras.layers.Input((None, input_dim))
    x = keras.layers.Reshape((-1, input_dim, 1))(input_spectrogram)
    x = ConvBlock()(x)
    x = keras.layers.Reshape((-1, x.shape[-2] * x.shape[-1]))(x)

    for i in range(1, rnn_layers + 1):
        gru = keras.layers.GRU(
            units=rnn_dims,
            return_sequences=True,
            reset_after=True,
            name=f"gru_{i}",
        )
        x = keras.layers.Bidirectional(
            gru, name=f"bidirectional_{i}", merge_mode="concat"
        )(x)
        if i < rnn_layers:
            x = keras.layers.Dropout(rate=dropout_rate)(x)

    x = keras.layers.Dense(units=fc_dims, activation="relu")(x)
    x = keras.layers.Dropout(rate=dropout_rate)(x)

    prob = keras.layers.Dense(units=output_dim + 1, activation="softmax")(x)

    model = keras.Model(input_spectrogram, prob, name="DeepSpeech_2")

    return model
