# asr/train.py

from argparse import Namespace
from pathlib import Path

import numpy as np
import tensorflow as tf
from tensorflow import keras
from sklearn.model_selection import train_test_split

from asr import models, data, utils, eval
from config import config


def ctc_loss(y_true, y_pred):

    batch_len = tf.cast(tf.shape(y_true)[0], dtype="int64")
    input_length = tf.cast(tf.shape(y_pred)[1], dtype="int64")
    label_length = tf.cast(tf.shape(y_true)[1], dtype="int64")

    input_length = input_length * tf.ones(shape=(batch_len, 1), dtype="int64")
    label_length = label_length * tf.ones(shape=(batch_len, 1), dtype="int64")

    loss = keras.backend.ctc_batch_cost(y_true, y_pred, input_length, label_length)
    return loss


class WERCallback(keras.callbacks.Callback):

    def __init__(self, dataset, index_to_char, greedy=True):
        super().__init__()
        self.dataset = dataset
        self.index_to_char = index_to_char
        self.greedy = greedy

    def on_train_begin(self, logs={}):
        self.best_wer_score = float("inf")

    def on_epoch_end(self, epoch: int, logs=None):
        wer_score = eval.get_wer(
            dataset=self.dataset,
            model=self.model,
            index_to_char=self.index_to_char,
            greedy=self.greedy
        )

        if self.best_wer_score > wer_score:
            self.best_wer_score = wer_score
            self.model.save(Path(config.STORES_DIR, "best_wer_model.h5"))


def train(params: Namespace):
    # set seed
    utils.set_seed(params.seed)
    # load data
    X, y = data.load_pickle(Path(config.DATA_DIR), "data.pickle")
    char2index, index2char = data.build_lookup(config.CHARACTERS)

    # Split data into training, validation and testing set
    X_train, X_, y_train, y_ = train_test_split(X, y, test_size=params.test_size)
    X_val, X_test, y_val, y_test = train_test_split(X_, y_, test_size=0.5)

    train_dataset = data.create_dataset(X_train, y_train, params.batch_size, train=True)
    val_dataset = data.create_dataset(X_val, y_val, params.batch_size)
    test_dataset = data.create_dataset(X_test, y_test, params.batch_size)

    # training settings
    ckpt_filepath = Path(config.STORES_DIR, "best_model.h5")
    #  TODO change monitor to WER metric
    ckpt_callback = keras.callbacks.ModelCheckpoint(
        ckpt_filepath,
        monitor="val_loss",
        save_best_only=True,
        mode="min"
    )

    # earlystop

    reduce_lr = keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss",
        mode="min",
        verbose=1,
        factor=0.05,
        patience=5
    )

    wer_callback = WERCallback(
        dataset=val_dataset,
        index_to_char=index2char,
        greedy=params.greedy,
    )

    callbacks = [ckpt_callback, reduce_lr, wer_callback]

    optimizer = keras.optimizers.Adam(learning_rate=params.lr)

    input_dim = X_train[0].shape[-1]

    model = models.initilize_model(
        input_dim=input_dim,
        output_dim=char2index.vocabulary_size(),
        rnn_layers=params.rnn_layers,
        rnn_dims=params.rnn_dims,
        fc_dims=params.fc_dims,
        dropout_rate=params.dropout_rate,
    )
    model.compile(optimizer=optimizer, loss=ctc_loss)

    # training & validation
    steps = len(y_train) // params.batch_size + 1
    history = model.fit(
        train_dataset,
        validation_data=val_dataset,
        epochs=params.epochs,
        callbacks=callbacks,
        steps_per_epoch=steps,
    )
    best_model = keras.models.load_model(Path(config.STORES_DIR, "best_wer_model.h5"), custom_objects={"ctc_Loss": ctc_loss})
    # TODO Loss
    # best_val_loss = history.history["val_loss"]
    artifacts = {
        "params": params,
        "model": best_model,
        "index_to_char": index2char,
        "char_to_index": char2index,
        # "loss": best_val_loss,
    }

    performance = eval.evaluate(dataset=test_dataset, artifacts=artifacts)

    return performance
