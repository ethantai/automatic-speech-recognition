# asr/eval.py

import numpy as np
import tensorflow as tf
from tensorflow import keras

from jiwer import wer

from config.config import logger


def decode(y_pred, index_to_char, greedy=True):
    input_len = np.ones(y_pred.shape[0]) * y_pred.shape[1]
    results = keras.backend.ctc_decode(y_pred, input_length=input_len, greedy=greedy)[0][0]
    output_text = []
    for result in results:
        result = tf.strings.reduce_join(index_to_char(result)).numpy().decode("utf-8")
        output_text.append(result)
    return output_text


def get_wer(dataset, model, index_to_char, greedy):
    predictions = []
    targets = []
    for batch in dataset:
        X, y = batch
        y_pred = model.predict(X)
        y_pred = decode(y_pred, index_to_char=index_to_char, greedy=greedy)
        predictions.extend(y_pred)
        for label in y:
            label = (
                tf.strings.reduce_join(index_to_char(label)).numpy().decode("utf-8")
            )
            targets.append(label)
    wer_score = wer(targets, predictions)
    logger.info(f"Word Error Rate: {wer_score:.4f}")
    for i in np.random.randint(0, len(predictions), 3):
        print(f"Target    : {targets[i]}")
        print(f"Prediction: {predictions[i]}")
        print("-" * 100)

    return wer_score


def evaluate(
        dataset,
        artifacts
):
    params = artifacts["params"]
    model = artifacts["model"]
    index2char = artifacts["index_to_char"]

    test_loss = model.evaluate(dataset)
    wer_score = get_wer(
        dataset=dataset,
        model=model,
        index_to_char=index2char,
        greedy=params.greedy
    )
    performance = {
        "loss": test_loss,
        "wer": wer_score,
    }

    return performance
