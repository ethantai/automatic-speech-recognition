# asr/predict.py

from typing import Dict

from asr import data, eval


def predict(audio_wav_file, artifacts: Dict):
    params = artifacts["params"]
    model = artifacts["model"]
    index2char = artifacts["index_to_char"]

    spectrogram = data.preprocess_audio(audio_wav_file)
    y_pred = model.predict(spectrogram)
    texts = eval.decode(y_pred=y_pred, index_to_char=index2char, greedy=params.greedy)

    return texts

