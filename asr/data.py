# asr/data.py

import pickle
from pathlib import Path
from argparse import Namespace

import tensorflow as tf
from tensorflow import keras

from config import config


def build_lookup(chars):
    characters = [c for c in chars]

    char2index = keras.layers.StringLookup(vocabulary=characters, oov_token="")
    index2char = keras.layers.StringLookup(
        vocabulary=char2index.get_vocabulary(),
        oov_token="",
        invert=True,
    )
    return char2index, index2char


def preprocess_audio(wav_filename):
    file = tf.io.read_file(wav_filename)
    audio, _ = tf.audio.decode_wav(file)
    audio = tf.squeeze(audio, axis=-1)
    audio = tf.cast(audio, tf.float32)
    spectrogram = tf.signal.stft(
        audio,
        frame_length=config.FRAME_LENGTH,
        frame_step=config.FRAME_STEP,
        fft_length=config.FFT_LENGTH,
    )
    spectrogram = tf.abs(spectrogram)
    spectrogram = tf.math.pow(spectrogram, 0.5)
    means = tf.math.reduce_mean(spectrogram, 1, keepdims=True)
    stddevs = tf.math.reduce_std(spectrogram, 1, keepdims=True)
    spectrogram = (spectrogram - means) / (stddevs + 1e-10)

    return spectrogram.numpy()


def preprocess_transcription(transcription, char_to_index):
    transcription = tf.strings.lower(transcription)
    transcription = tf.strings.unicode_split(transcription, input_encoding="UTF-8")
    label = char_to_index(transcription)
    return label.numpy()


def prepare_data(df):
    data = []
    labels = []
    char2index, index2char = build_lookup(config.CHARACTERS)

    for audio_file, texts in zip(df['file_name'], df["normalized_transcription"]):
        spectrogram = preprocess_audio(Path(config.LJSPEECH_DATA_DIR, audio_file + ".wav"))
        transcription = preprocess_transcription(texts, char2index)

        data.append(spectrogram)
        labels.append(transcription)

    return data, labels
    # data = [preprocess(*(*a, char2index)) for a in tuple(zip(df['file_name'], df["normalized_transcription"]))]


def save_pickle(data, filepath):
    with open(filepath, "wb") as pickle_file:
        pickle.dump(data, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)


def load_pickle(filepath):
    with open(filepath, "rb") as pickle_file:
        return pickle_file.load(pickle_file)


def create_dataset(X, y, batch_size, train=False):
    dataset = tf.data.Dataset.from_generator(
        lambda: iter(zip(X, y)),
        output_types=(tf.float32, tf.int64)
    ).padded_batch(batch_size, padded_shapes=([None, None], [None])).prefetch(buffer_size=tf.data.AUTOTUNE)
    if train:
        dataset = dataset.repeat()

    return dataset
