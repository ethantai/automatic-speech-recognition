# asr/main.py

import json
from argparse import Namespace
from pathlib import Path
from typing import Dict, Optional
import tempfile

import mlflow
from tensorflow import keras
import typer

from config import config
from config.config import logger
from asr import utils, train, predict

app = typer.Typer()


@app.command()
def download_ljspeech_data():
    data_url = "https://data.keithito.com/data/speech/LJSpeech-1.1.tar.bz2"
    downloaded_data_path = keras.utils.get_file(
        fname="LJSpeech-1.1",
        origin=data_url,
        untar=True,
        cache_subdir=config.DATA_DIR
    )

    logger.info("LJSpeech data downloaded!")
    config.LJSPEECH_DATA_DIR = downloaded_data_path


@app.command()
def train_model(
        params_filepah: Path = Path(config.CONFIG_DIR, "params.json"),
        experiment_name: Optional[str] = "best",
        run_name: Optional[str] = "model"

) -> None:

    params = Namespace(**utils.load_dict(params_filepah))

    mlflow.set_experiment(experiment_name=experiment_name)
    with mlflow.start_run(run_name=run_name):
        run_id = mlflow.active_run().info.run_id
        logger.info(f"Run ID: {run_id}")

        artifacts = train.train(params=params)
        performance = artifacts["performance"]
        logger.info(json.dumps(performance, indent=2))

        with tempfile.TemporaryDirectory() as dp:
            utils.save_dict(vars(artifacts["params"]), Path(dp, "params.json"))
            utils.save_dict(performance, Path(dp, "performance.json"))
            utils.save_dict(artifacts["index_to_char"].get_config(), Path(dp, "index2char.json"))
            artifacts["model"].save(Path(dp, "model.h5"))
            mlflow.log_artifacts(artifacts)
        mlflow.log_params(vars(artifacts["params"]))


@app.command()
def speech_to_text(audio_file, run_id: str):
    artifacts = load_artifacts(run_id=run_id)
    prediction = predict.predict(audio_wav_file=audio_file, artifacts=artifacts)
    #  TODO prediction dataformat
    logger.info(prediction)

    return prediction


@app.command()
def params(run_id: str) -> Dict:
    params = load_artifacts(run_id=run_id)["params"]
    logger.info(json.dumps(params, indent=2))
    return params


@app.command()
def performance(run_id: str) -> Dict:
    performance = load_artifacts(run_id=run_id)["performance"]
    logger.info(json.dumps(performance, indent=2))
    return performance


def load_artifacts(run_id: str) -> Dict:
    artifact_uri = mlflow.get_run(run_id=run_id).info.artifact_uri.split("file://")[-1]
    params = Namespace(**utils.load_dict(filepath=Path(artifact_uri, "params.json")))
    model = keras.models.load_model(Path(artifact_uri, "model.h5"), custom_objects={"ctc_loss": train.ctc_loss})
    performance = utils.load_dict(filepath=Path(artifact_uri, "performance.json"))
    index_to_char_dict = utils.load_dict(filepath=Path(artifact_uri, "index2char.json"))
    index_to_char = keras.layers.StringLookup.from_config(index_to_char_dict)

    return {
        "params": params,
        "model": model,
        "performance": performance,
        "index_to_char": index_to_char
    }


def delete_experiment(experiment_name: str):
    client = mlflow.tracking.MlflowClient()
    experiment_id = client.get_experiment_by_name(experiment_name).experiment_id
    client.delete_experiment(experiment_id=experiment_id)
    logger.info(f"Deleted experiment {experiment_name}!")

