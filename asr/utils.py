# asr/utils.py

import json
import random
from typing import Dict

import numpy as np
import tensorflow as tf


def set_seed(seed: int = 2022) -> None:
    np.random.seed(seed)
    random.seed(seed)
    tf.random.set_seed(seed)


def load_dict(filepath: str) -> Dict:
    with open(filepath) as fp:
        dict_ = json.load(fp)
    return dict_


def save_dict(d: Dict, filepath: str, cls=None, sortkeys: bool = False) -> None:
    with open(filepath, "w") as fp:
        json.dump(d, indent=2, fp=fp, cls=cls, sort_keys=sortkeys)


