# setup.py

from setuptools import find_namespace_packages, setup

setup(
    name="asr",
    version="0.1",
    python_requires=">=3.7",
    packages=find_namespace_packages(),
    entry_points={
        "console_scripts": [
            "asr = asr.main:app",
        ],
    },
)