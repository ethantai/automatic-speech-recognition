# Automatic Speech Recognition
MLOps for automatic speech recognition  
## Directory structure
```sh
asr/
├── data.py         - data preprocessing
├── eval.py         - evaluation
├── main.py         - training pipelines
├── models.py       - model architectures
├── predict.py      - inference
├── train.py        - training components
└── utils.py        - utilities
config/             
├── config.py       - configuration setups
└── params.json     - training parameters
```

## Workflows

1. Set up environment.
2. Download LJSpeech data
```sh
asr download_ljspeech_data
```
3. Train a model with parameters from [config/params.json](../blob/master/config/params.json).
```sh
asr train_model
```

## TODO
- [ ] Documentation
- [ ] FastAPI
- [ ] Docker
- [ ] Streamlit app 


